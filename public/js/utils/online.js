function online() {
    var userAgent = window.navigator.userAgent;

    var browser = "";
    if ((vOffset = userAgent.indexOf("Opera")) != -1) {
        browser = "Opera";
    } else if ((vOffset = userAgent.indexOf("Chrome")) != -1) {
        browser = "Chrome";
    } else if ((vOffset = userAgent.indexOf("Safari")) != -1) {
        browser = "Safari";
    } else if ((vOffset = userAgent.indexOf("MSIE")) != -1) {
        browser = "Internet Explorer";
    } else if ((vOffset = userAgent.indexOf("Firefox")) != -1) {
        browser = "Firefox";
    } else {
        browser = "Other";
    }

    var ip = $("meta[name='online_counter']").attr("ip");
    var onlineUrl = $("meta[name='online_counter']").attr("url");
    var token = $("meta[name='online_counter']").attr("token");

    var dataArray = {
        "ip": ip,
        "browser": browser,
        "_token": token
    };
    if ($.cookie("client_id") !== undefined) {
        dataArray["client_id"] = $.cookie("client_id");
    }

    if($.cookie("expire_time") === undefined)
    {
        $.ajax({
            type: "post",
            url: onlineUrl,
            data: dataArray,
            success: function (data) {
                var expire_date = new Date();
                console.log(data.expire_time);
                var minutes = data.expire_time;
                expire_date.setTime(expire_date.getTime() + (minutes * 60 * 1000));

                $.cookie("client_id", data.client_id, {path: "/"});
                $.cookie("expire_time", expire_date, {path: "/"});
            }
        });
    }
    else if(new Date($.cookie("expire_time")).getTime() < new Date().getTime()){
        $.ajax({
            type: "post",
            url: onlineUrl,
            data: dataArray,
            success: function (data) {
                var expire_date = new Date();
                var minutes = data.expire_time;
                expire_date.setTime(expire_date.getTime() + (minutes * 60 * 1000));

                $.cookie("client_id", data.client_id, {path: "/", secure: true});
                $.cookie("expire_time", expire_date, {path: "/", secure: true});
            }
        });
    }
}