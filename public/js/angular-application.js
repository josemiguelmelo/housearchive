
var application = angular.module('application', ['ngCookies', 'angularUtils.directives.dirPagination']).config(function($interpolateProvider){
    $interpolateProvider.startSymbol('<%').endSymbol('%>');
});


application.service('CookiesService', ['$cookies', function($cookies){

    this.putObject = function(name, object){
        $cookies.putObject(name, object,{
            path: "/"
        });
    };

    this.getObject = function(name){
        return $cookies.get(name);
    }
}]);


application.service('HTTPService', ['$http', function($http){

    this.post = function(url, params){
        return $http({method: "post", url:url, params: params}).
        then(function(response){
            return response;
        });
    };

    this.get = function(url){
        return $http({method: "get", url:url}).
        then(function successCallback(response){
            return response;
        }, function errorCallback(response){
            return false;
        });
    };


    this.getWithParams = function(url, params){
        return $http({method: "get", url:url, params: params}).
        then(function(response){
            return response;
        });
    };
}]);




application.filter('groupBy', ['$parse', 'pmkr.filterStabilize', function ($parse, filterStabilize) {

    function groupBy(input, prop) {

        if (!input) { return; }

        var grouped = {};

        input.forEach(function(item) {
            var key = $parse(prop)(item);
            grouped[key] = grouped[key] || [];
            grouped[key].push(item);
        });

        return grouped;

    }

    return filterStabilize(groupBy);

}])

    .factory('pmkr.filterStabilize', [
        'pmkr.memoize',
        function(memoize) {

            function service(fn) {

                function filter() {
                    var args = [].slice.call(arguments);
                    // always pass a copy of the args so that the original input can't be modified
                    args = angular.copy(args);
                    // return the `fn` return value or input reference (makes `fn` return optional)
                    var filtered = fn.apply(this, args) || args[0];
                    return filtered;
                }

                var memoized = memoize(filter);

                return memoized;

            }

            return service;

        }
    ])

    .factory('pmkr.memoize', [
        function() {

            function service() {
                return memoizeFactory.apply(this, arguments);
            }

            function memoizeFactory(fn) {

                var cache = {};

                function memoized() {

                    var args = [].slice.call(arguments);

                    var key = JSON.stringify(args);

                    if (cache.hasOwnProperty(key)) {
                        return cache[key];
                    }

                    cache[key] = fn.apply(this, arguments);

                    return cache[key];

                }

                return memoized;

            }

            return service;

        }
    ])


;