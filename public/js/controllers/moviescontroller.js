

application.controller("MoviesController", function($scope, CookiesService, HTTPService) {
    $scope.filteredTodos = []
        ,$scope.currentPage = 1
        ,$scope.numPerPage = 10
        ,$scope.maxSize = 5;

    $scope.getMovies = function() {
        var response = HTTPService.get("/api/movies");

        response.then(function(result){
            $scope.movies = result.data;
        });
    };
    $scope.getMovies();


});