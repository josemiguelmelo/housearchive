

application.controller("BooksController", function($scope, CookiesService, HTTPService) {
    $scope.filteredTodos = []
        ,$scope.currentPage = 1
        ,$scope.numPerPage = 10
        ,$scope.maxSize = 5;

    $scope.getBooks = function() {
        var response = HTTPService.get("/api/books");

        response.then(function(result){
            $scope.books = result.data;
        });
    };
    $scope.getBooks();


});