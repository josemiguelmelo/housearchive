

application.controller("SerieController", function($scope, $sce, CookiesService, HTTPService) {
    $scope.filteredTodos = []
        ,$scope.currentPage = 1
        ,$scope.numPerPage = 10
        ,$scope.maxSize = 5;

    $scope.getSerie = function() {
        var id = $("#serie-id").data("id");
        var serieResult = HTTPService.get("http://api.tvmaze.com/shows/" + id + "?embed=episodes");
        serieResult.then(function(serieData)
        {
            var serie = {
                'id' : id,
                'description' : $sce.trustAsHtml(serieData.data.summary),
                'name' : serieData.data.name,
                'rating' : serieData.data.rating.average,
                'episodes' : serieData.data._embedded.episodes,
                'image' : serieData.data.image.original
            };
            $scope.serie = serie;

            console.log($scope.serie);
        });
    };

    $scope.getSerie();


});