

application.controller("MusicController", function($scope, CookiesService, HTTPService) {
    $scope.filteredTodos = []
        ,$scope.currentPage = 1
        ,$scope.numPerPage = 10
        ,$scope.maxSize = 5;

    $scope.getMusics = function() {
        var response = HTTPService.get("/api/music");

        response.then(function(result){
            $scope.musics = result.data;
        });
    };
    $scope.getMusics();


});