

application.controller("SeriesController", function($scope, $sce, CookiesService, HTTPService) {
    $scope.filteredTodos = []
        ,$scope.currentPage = 1
        ,$scope.numPerPage = 10
        ,$scope.maxSize = 5;

    $scope.events = [];


    $scope.initCalendar = function()
    {
        /*
         date store today date.
         d store today date.
         m store current month.
         y store current year.
         */
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        /*
         Initialize fullCalendar and store into variable.
         Why in variable?
         Because doing so we can use it inside other function.
         In order to modify its option later.
         */

        var calendar = $('#calendar').fullCalendar(
            {
                /*
                 header option will define our calendar header.
                 left define what will be at left position in calendar
                 center define what will be at center position in calendar
                 right define what will be at right position in calendar
                 */
                header:
                {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                /*
                 defaultView option used to define which view to show by default,
                 for example we have used agendaWeek.
                 */
                defaultView: 'agendaWeek',
                /*
                 selectable:true will enable user to select datetime slot
                 selectHelper will add helpers for selectable.
                 */
                selectable: true,
                selectHelper: true,
                /*
                 when user select timeslot this option code will execute.
                 It has three arguments. Start,end and allDay.
                 Start means starting time of event.
                 End means ending time of event.
                 allDay means if events is for entire day or not.
                 */
                select: function(start, end, allDay)
                {
                    /*
                     after selection user will be promted for enter title for event.
                     */
                    var title = prompt('Event Title:');
                    /*
                     if title is enterd calendar will add title and event into fullCalendar.
                     */
                    if (title)
                    {
                        calendar.fullCalendar('renderEvent',
                            {
                                title: title,
                                start: start,
                                end: end,
                                allDay: allDay
                            },
                            true // make the event "stick"
                        );
                    }
                    calendar.fullCalendar('unselect');
                },
                /*
                 editable: true allow user to edit events.
                 */
                editable: true,
                /*
                 events is the main option for calendar.
                 for demo we have added predefined events in json object.
                 */
                events: $scope.events
            });

    };

    $scope.getSeries = function() {
        var response = HTTPService.get("/api/series");

        response.then(function(result){
            var seriesIds = result.data;
            var i ;

            $scope.series = [];


            for (i = 0; i < seriesIds.length; i++)
            {
                var serieResult = HTTPService.get("http://api.tvmaze.com/shows/" + seriesIds[i].tvmaze_id + "?embed=episodes");
                serieResult.then(function(serieData)
                {
                    if(serieData.data._links.nextepisode == null)
                    {
                        var serie = {
                            'id': serieData.data.id,
                            'description': $sce.trustAsHtml(serieData.data.summary),
                            'name': serieData.data.name,
                            'rating': serieData.data.rating.average,
                            'episodes': serieData.data._embedded.episodes,
                            'image': serieData.data.image.original
                        };

                        $scope.series.push(serie);
                    }else {
                        var nextEpisode = HTTPService.get(serieData.data._links.nextepisode.href);


                        if (nextEpisode == false) {
                            var serie = {
                                'id': serieData.data.id,
                                'description': $sce.trustAsHtml(serieData.data.summary),
                                'name': serieData.data.name,
                                'rating': serieData.data.rating.average,
                                'episodes': serieData.data._embedded.episodes,
                                'image': serieData.data.image.original,
                                'next_episode': nextEpData.data.airdate
                            };

                            $scope.series.push(serie);
                        } else {
                            nextEpisode.then(function (nextEpData) {

                                var serie = {
                                    'id': serieData.data.id,
                                    'description': $sce.trustAsHtml(serieData.data.summary),
                                    'name': serieData.data.name,
                                    'rating': serieData.data.rating.average,
                                    'episodes': serieData.data._embedded.episodes,
                                    'image': serieData.data.image.original,
                                    'next_episode': nextEpData.data.airdate
                                };

                                $scope.series.push(serie);

                                var from = serie.next_episode.split("-");
                                var date = new Date(from[0], from[1] - 1, from[2], nextEpData.data.airtime.split(":")[0]);
                                var date_end = new Date(from[0], from[1] - 1, from[2], parseInt(nextEpData.data.airtime.split(":")[0]) + 1);
                                $scope.events.push({
                                    title: serie.name,
                                    start: date,
                                    end: date_end,
                                    color: getRandomColor()
                                });

                            });
                        }
                    }
                });
            }
        });
    };

    function getRandomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    $scope.getSeries();

    $scope.searchBar = "";



    $scope.$watch('searchBar', function (newValue, oldValue) {
        search();
    });

    function search() {
        var seriesResult = HTTPService.get("http://api.tvmaze.com/search/shows?q=" + $scope.searchBar.split(' ').join('+') );
        seriesResult.then(function(seriesData)
        {
            var data = seriesData.data;
            var i;

            $scope.searchResults = [];
            console.log(data);
            for (i = 0; i < data.length; i++)
            {
                var result = {
                    'id' : data[i].show.id,
                    'url' : data[i].show.url,
                    'name' : data[i].show.name,
                    'rating' : data[i].show.rating.average,
                    'image' : data[i].show.image.original,
                    'summary' : data[i].show.summary
                }
                $scope.searchResults.push(result);
            }
        });

    };
});