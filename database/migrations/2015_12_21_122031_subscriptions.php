<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subscriptions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('subscriptions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('books');
			$table->integer('music');
			$table->integer('movies');
			$table->float('monthly_price');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subscriptions');
	}

}
