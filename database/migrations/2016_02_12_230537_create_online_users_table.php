<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('online_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('ip_address');
			$table->integer('user_id')->unsigned()->nullable()->default(null);
			$table->foreign('user_id')->references('id')->on('users');
			$table->dateTime('expire_time');
			$table->string('browser');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('online_users');
	}

}
