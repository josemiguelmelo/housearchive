<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SubscriptionsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \HouseArchive\Subscription::create(
            [
                'name' => 'Free',
                'books' => 200,
                'music' => 200,
                'movies' => 200,
                'monthly_price' => 0.0,
                'default' => true
            ]
        );
        \HouseArchive\Subscription::create(
            [
                'name' => 'Silver',
                'books' => 750,
                'music' => 750,
                'movies' => 750,
                'monthly_price' => 5.0,
            ]
        );
        \HouseArchive\Subscription::create(
            [
                'name' => 'Gold',
                'books' => 5000,
                'music' => 5000,
                'movies' => 5000,
                'monthly_price' => 15.0
            ]
        );
    }

}
