<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdminSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = \Illuminate\Support\Facades\Hash::make('password');


        \HouseArchive\User::create([
            'name' => 'Admin',
            'email' => 'admin@housearchive.com',
            'password' => $password,
            'admin' => true
        ]);
    }

}
