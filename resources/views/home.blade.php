@extends('app')

@section('content')


	<div class="row">
		<div class="row">

			<div class=" col-md-3 col-md-offset-1">
				<a href="{{ route('books') }}">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3>Books</h3>
							<div class="text-right"><i class="fa fa-book fa-5x"></i></div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3 col-md-offset-1">
				<a href="{{ route('music') }}">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3>Music</h3>
							<div class="text-right"><i class="fa fa-music fa-5x"></i></div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3 col-md-offset-1">
				<a href="{{ route('movies') }}">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3>Movies</h3>
							<div class="text-right"><i class="fa fa-film fa-5x"></i></div>
						</div>
					</div>
				</a>
			</div>

		</div>
	</div>
	<hr>


	<div class="row">
		<div class="row">
			<div class=" col-md-3 col-md-offset-1"></div>
			<div class=" col-md-3 col-md-offset-1">
				<a href="{{ route('series') }}">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3>Series</h3>
							<div class="text-right"><i class="fa fa-television fa-5x"></i></div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
	@if(count(Auth::user()->books) > 0 || count(Auth::user()->music) > 0 || count(Auth::user()->movies) > 0)
	<hr>

	<div class="row">

		<div class="row">
			<div class=" col-md-5 col-md-offset-1">
				<h3>Books vs Music vs Movies</h3>
				<br>
				<canvas class="text-center" id="bvsmvsm" width="400" height="400"></canvas>
			</div>
		</div>
	</div>
	@endif


	<script>
		var dataBMM = [
			{
				value: "{{ count(Auth::user()->books) }}",
				color:"#F7464A",
				highlight: "#FF5A5E",
				label: "Books"
			},
			{
				value: "{{ count(Auth::user()->music) }}",
				color: "#46BFBD",
				highlight: "#5AD3D1",
				label: "Music"
			},
			{
				value: "{{ count(Auth::user()->movies) }}",
				color: "#FDB45C",
				highlight: "#FFC870",
				label: "Movies"
			}
		];
		var options = {
			//Boolean - Whether we should show a stroke on each segment
			segmentShowStroke : true,

			//String - The colour of each segment stroke
			segmentStrokeColor : "#fff",

			//Number - The width of each segment stroke
			segmentStrokeWidth : 2,

			//Number - The percentage of the chart that we cut out of the middle
			percentageInnerCutout : 50, // This is 0 for Pie charts

			//Number - Amount of animation steps
			animationSteps : 100,

			//String - Animation easing effect
			animationEasing : "easeOutBounce",

			//Boolean - Whether we animate the rotation of the Doughnut
			animateRotate : true,

			//Boolean - Whether we animate scaling the Doughnut from the centre
			animateScale : false
		};


		var dataShelfs = {
			labels: ["January", "February", "March", "April", "May", "June", "July"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.5)",
					strokeColor: "rgba(220,220,220,0.8)",
					highlightFill: "rgba(220,220,220,0.75)",
					highlightStroke: "rgba(220,220,220,1)",
					data: [65, 59, 80, 81, 56, 55, 40]
				},
				{
					label: "My Second dataset",
					fillColor: "rgba(151,187,205,0.5)",
					strokeColor: "rgba(151,187,205,0.8)",
					highlightFill: "rgba(151,187,205,0.75)",
					highlightStroke: "rgba(151,187,205,1)",
					data: [28, 48, 40, 19, 86, 27, 90]
				}
			]
		};
		var ctx = document.getElementById("bvsmvsm").getContext("2d");
		//var ctx2 = document.getElementById("myChart").getContext("2d");
		var myPieChart = new Chart(ctx).Pie(dataBMM,options);
		//var myPieChart2 = new Chart(ctx2).Bar(dataBMM,options);
	</script>

@endsection
