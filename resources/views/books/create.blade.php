@extends('app')

@section('content')

    @include('static.go_back', ['url' => route('books')])

    <hr>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New Book</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('books.store') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">ISBN</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="isbn" id="isbn" value="{{ old('isbn') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Title</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Author</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="author" id="author" value="{{ old('author') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Publisher</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="publisher" id="publisher" value="{{ old('publisher') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Date Published</label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="release-date" id="release-date" value="{{ old('release-date') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Description</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="description" id="description">{{ old('description') }}</textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">Shelf</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="shelf" id="shelf" value="{{ old('shelf') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Image</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="image" id="image" value="{{ old('image') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Image Preview</label>
                            <div class="col-md-6">
                                <img id="image-preview" src=""/>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>

    <script>
        $("#image").change(function(){
            $("#image-preview").attr("src", $(this).val());
        });

        var googleBooksAPI = "https://www.googleapis.com/books/v1/volumes?q=isbn:";

        $("#isbn").change(function(){
            var isbn = $("#isbn").val();
            $.ajax({
                url: googleBooksAPI + isbn,
                type: "GET",
                success: function(data)
                {
                    var volumeInfo = data.items[0].volumeInfo;

                    $("#title").val(volumeInfo.title);
                    $("#author").val(volumeInfo.authors[0]);
                    $("#publisher").val(volumeInfo.publisher);
                    $("#release-date").val(volumeInfo.publishedDate);
                    $("#description").val(volumeInfo.description);
                    $("#image").val(volumeInfo.imageLinks.thumbnail);
                    $("#image-preview").attr("src", volumeInfo.imageLinks.thumbnail);
                }
            });
        });

    </script>

@endsection
