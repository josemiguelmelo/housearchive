@extends('app')

@section('content')
    <style>
        body { margin-top:20px; }
        .panel-title {display: inline;font-weight: bold;}
        .checkbox.pull-right { margin: 0; }
        .pl-ziro { padding-left: 0px; }
    </style>

    <div class="row">
        <div class="col-md-5 col-md-offset-1">
            <h2>Subscription</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-1">
            <h4>Current Plan</h4>
        </div>
        <div class="col-md-6 col-md-offset-1">
            <h4>Change Plan</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-4 col-md-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4>{{ Auth::user()->currentSubscription->name }}</h4>
                </div>


                <div class="panel-body">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label">Books </label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Music </label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Movies </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="label label-primary">
                                    {{ count(Auth::user()->books) }} / {{ Auth::user()->currentSubscription->books }}
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="label label-primary">
                                    {{ count(Auth::user()->music) }} / {{ Auth::user()->currentSubscription->music }}
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="label label-primary">
                                    {{ count(Auth::user()->movies) }} / {{ Auth::user()->currentSubscription->movies }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Payment Details
                    </h3>
                </div>
                <div class="panel-body">
                    <form role="form" id="payment-form" action="{{ route('subscription') }}" method="POST">
                        <span class="payment-errors alert alert-danger" hidden></span>
                        <div class="form-group">
                            <label for="cardNumber">
                                SUBSCRIPTION</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-credit-card"></span></span>
                                <select type="text" class="form-control" id="subscriptionPlan" required autofocus name="subscription_plan">
                                    @foreach(\HouseArchive\Subscription::all() as $subscription)
                                        <option value="{{ $subscription->code }}" data-sub_price="{{ $subscription->monthly_price }}">{{ $subscription->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="cardNumber">
                                CARD NUMBER</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="cardNumber" placeholder="Valid Card Number"
                                       required autofocus data-stripe="number"/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="expityMonth">EXPIRY DATE</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6 col-md-6 col-lg-6">
                                            <input type="text" class="form-control" data-stripe="exp_month" id="expityMonth" placeholder="MM" required />
                                        </div>

                                        <div class="col-xs-6 col-md-6 col-lg-6">
                                            <input type="text" class="form-control" data-stripe="exp_year" id="expityYear" placeholder="YY" required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-5 col-md-5 pull-right">
                                <div class="form-group">
                                    <label for="cvCode">
                                        CVC CODE</label>
                                    <input type="password" class="form-control" data-stripe="cvc" id="cvCode" placeholder="CVC" required />
                                </div>
                            </div>
                        </div>

                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a href="#"><span class="badge pull-right"><span class="glyphicon glyphicon-usd"></span><span id="monthlyPrice"></span></span> Monthly Price</a>
                            </li>
                        </ul>
                        <br/>
                        <button class="btn btn-success btn-lg btn-block">Pay</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script>
        var currentPrice = 0;
        function updatePrice(){
            $("#monthlyPrice").text(currentPrice);
        }
        $(document).ready(function(){
            currentPrice = $(this).find(':selected').data('sub_price');
            updatePrice();
        });
        $("#subscriptionPlan").change(function(){
            currentPrice = $(this).find(':selected').data('sub_price');
            updatePrice();
        });

        Stripe.setPublishableKey("{{ env('STRIPE_API_PUBLIC') }}");

        $('#payment-form').submit(function(event) {
            var $form = $(this);
            // Disable the submit button to prevent repeated clicks:
            $form.find('.submit').prop('disabled', true);

            // Request a token from Stripe:
            Stripe.card.createToken($form, stripeResponseHandler);

            // Prevent the form from being submitted:
            return false;
        });
        function stripeResponseHandler(status, response) {
            var $form = $('#payment-form');

            if (response.error) { // Problem!

                // Show the errors on the form:
                $form.find('.payment-errors').text(response.error.message);
                $form.find('.payment-errors').show();

                $form.find('.submit').prop('disabled', false); // Re-enable submission

            } else { // Token was created!

                // Get the token ID:
                var token = response.id;

                console.log("HREE");

                // Insert the token ID into the form so it gets submitted to the server:
                $form.append($('<input type="hidden" name="stripe_token">').val(token));

                // Submit the form:
                $form.get(0).submit();
            }
        };

    </script>
@endsection
