<div class="row">
    <div class="row">
        <div class="col-md-5 col-md-offset-1">
            <h4>Subscription Plan</h4>
        </div>

        <div class="col-md-5 col-md-offset-1">
            <a class="btn btn-primary btn-sm" href="{{ route('subscription.change') }}">Change plan</a>
        </div>
    </div>
    <hr>

    <div class="row">
        @if (Session::has('subscription_success'))
            <div class="alert alert-success col-md-offset-1 col-md-10">
                {{Session::get('subscription_success')}}
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-md-9 col-md-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4>{{ Auth::user()->currentSubscription->name }}</h4>
                </div>


                <div class="panel-body">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label">Books </label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Music </label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Movies </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="label label-primary">
                                    {{ count(Auth::user()->books) }}
                                    /
                                    {{ Auth::user()->currentSubscription->books == -1 ? 'Unlimited' : Auth::user()->currentSubscription->books}}
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="label label-primary">
                                    {{ count(Auth::user()->music) }}
                                    /
                                    {{ Auth::user()->currentSubscription->music == -1 ? 'Unlimited' : Auth::user()->currentSubscription->music}}
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="label label-primary">
                                    {{ count(Auth::user()->movies) }}
                                    /
                                    {{ Auth::user()->currentSubscription->movies == -1 ? 'Unlimited' : Auth::user()->currentSubscription->movies}}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>