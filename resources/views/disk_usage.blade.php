<div class="row">
    <div class="row">
        <div class="col-md-5 col-md-offset-1">
            <h4>Disk Usage</h4>
        </div>

        <div class="col-md-5 col-md-offset-1">
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="panel panel-info">
                <div class="panel-heading text-center">
                    <h4>Books Space Usage</h4>
                </div>
                <div class="panel-body text-center">
                    @include('disk_usage_progress_bar', ['resource' => 'books'])
                </div>
            </div>
        </div>

        <div class="col-md-3 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">
                    <h4>Music Space Usage</h4>
                </div>
                <div class="panel-body text-center">
                    @include('disk_usage_progress_bar', ['resource' => 'music'])
                </div>
            </div>
        </div>

        <div class="col-md-3 col-md-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading text-center">
                    <h4>Movies Space Usage</h4>
                </div>
                <div class="panel-body text-center">
                    @include('disk_usage_progress_bar', ['resource' => 'movies'])
                </div>
            </div>
        </div>
    </div>
</div>