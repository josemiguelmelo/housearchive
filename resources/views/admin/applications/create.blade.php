@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Create Application</h3>
        </div>
    </div>
    <hr>

    <div class="row">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form id="create-app-form" class="form-horizontal" role="form" method="POST" action="{{ route('admin.application.store') }}">

            <div class="form-group">
                <label class="col-md-4 control-label">Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="name" placeholder="Application name">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">ID</label>
                <div class="col-md-6">
                    <input id="id-input" type="text" class="form-control" name="id" value="{{ md5(uniqid(rand(), true)) }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Secret</label>
                <div class="col-md-6">
                    <input id="secret-input" type="text" class="form-control" name="secret" value="{{ md5(uniqid(rand(), true)) }}" disabled>
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>
                </div>
            </div>
        </form>
    </div>

    <script>
        $(function() {
            $('#create-app-form').submit(function() {
                $('#secret-input').attr('disabled', false);
                $('#id-input').attr('disabled', false);
                return true; // return false to cancel form action
            });
        });
    </script>

@stop