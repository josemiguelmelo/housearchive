@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <h3>Applications</h3>
        </div>
        <div class="col-md-2">
            <a href="{{ route('admin.application.create') }}" class="btn btn-primary">Create</a>
        </div>
    </div>
    <hr>
    @if (isset($success))
        <div class="alert alert-success">
            <br>{{ $success }}<br>
        </div>
    @endif
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table id="users" class="table text-sm table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>ID</th>
                    <th>Secret</th>
                    <th>Date</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>ID</th>
                    <th>Secret</th>
                    <th>Date</th>
                    <th>Options</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach($applications as $app)
                    <tr style="font-size: 80%;">
                        <td>{{ $app->name }}</td>
                        <td>{{ $app->id }}</td>
                        <td align="center">{{ $app->secret }}</td>
                        <td align="center">{{ $app->created_at }}</td>
                        <td align="right">
                            <div class="btn-toolbar" role="toolbar" aria-label="...">
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                    <a href="{{ route('admin.application.destroy', array('id' => $app->id)) }}" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    </div>

@stop