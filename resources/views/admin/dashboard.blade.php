@extends('admin.app')

@section('content')


    <!--- Books, movies and music stats --->

    <div class="row">
        <div class="col-md-12">
            <h3>Resources Stats</h3>
        </div>
    </div>


    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Books</h3>
                    <h4>{{ count(\HouseArchive\Book::all()) }}</h4>
                    <div class="text-right"><i class="fa fa-book fa-5x"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Music</h3>
                    <h4>{{ count(\HouseArchive\Music::all()) }}</h4>
                    <div class="text-right"><i class="fa fa-music fa-5x"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Movies</h3>
                    <h4>{{ count(\HouseArchive\Movies::all()) }}</h4>
                    <div class="text-right"><i class="fa fa-film fa-5x"></i></div>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <!--- User Stats --->

    <div class="row">
        <div class="col-md-12">
            <h3>Users Stats</h3>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-body">
                    <h4><i class="fa fa-circle text-success"></i> Online Users</h4>
                    <h3>{{ count(DB::table('online_users')->where('expire_time' , '>=', \Carbon\Carbon::now())->get()) }}</h3>
                    <div class="text-right"><i class="fa fa-user fa-5x text-success"></i></div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-body">
                    <h4><a href="{{ route('admin.users') }}" class="text-primary">Registered Users</a> </h4>
                    <h3>{{ count(\HouseArchive\User::all()) }}</h3>
                    <div class="text-right"><i class="fa fa-users fa-5x"></i></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    Users per Subscription
                </div>
                <div class="panel-body">
                    <div class="row">
                    @foreach(\HouseArchive\Subscription::all() as $subscription)
                        <div class="col-md-6">
                            <h4><a href="{{ route('admin.users') }}" class="text-primary"></a> {{$subscription->name}} </h4>
                            <h3>{{ count(\HouseArchive\User::where('subscription_id', '=', $subscription->id)->get()) }}</h3>
                            <hr>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-body">
                    <h4>Today Registrations</h4>
                    <h3>{{ count(\HouseArchive\User::where('created_at','>=', \Carbon\Carbon::today())->get()) }}</h3>
                    <div class="text-right"><i class="fa fa-sun-o fa-5x text-warning"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-body">
                    <h4>Last Month Registrations</h4>
                    <h3>{{ count(\HouseArchive\User::where('created_at','>=', \Carbon\Carbon::now()->addMonth(-1))->get()) }}</h3>
                    <div class="text-right"><i class="fa fa-calendar-o fa-5x text-info"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-body">
                    <h4>Last Year Registrations</h4>
                    <h3>{{ count(\HouseArchive\User::where('created_at','>=', \Carbon\Carbon::now()->addYear(-1))->get()) }}</h3>
                    <div class="text-right"><i class="fa fa-calendar fa-5x"></i></div>
                </div>
            </div>
        </div>
    </div>

@endsection
