<html>
<head>
    <meta name="online_counter" ip="{{ $_SERVER['REMOTE_ADDR'] }}" url="{{ route('online') }}" token="{{ csrf_token() }}">

    <title>HouseArchive</title>

    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
    <link href='/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/font-awesome.min.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">


    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery.cookie.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/chartjs.min.js"></script>
    <script src="/js/angular/angular.min.js"></script>
    <script src="/js/angular/angular-cookies.min.js"></script>
    <script src="/js/angular/dir-paginate.js"></script>
    <script src="/js/angular-application.js"></script>
    <script src="/js/controllers/bookscontroller.js"></script>
    <script src="/js/controllers/moviescontroller.js"></script>
    <script src="/js/controllers/musiccontroller.js"></script>
    <script src="/js/controllers/seriescontroller.js"></script>
    <script src="/js/controllers/serieController.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="/js/moment.min.js"></script>

    <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>


    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.5.0/fullcalendar.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.5.0/fullcalendar.min.js"></script>


    <script src="/js/utils/online.js"></script>
    <script>
        online();
    </script>

    <style>
        body { padding-top: 160px; }
    </style>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('admin.dashboard') }}">HouseArchive</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check())
                    @if(Auth::user()->admin)
                        <li><a href="{{ route('home') }}">Normal user</a></li>
                    @endif
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('profile') }}">Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ url('auth/logout') }}">
                                    <i class="fa fa-power-off"></i> Logout
                                </a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="{{ url('auth/login') }}">Sign In</a></li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->

        <div class="navbar">
            <ul class="nav navbar-nav">
                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ route('admin.users') }}"><i class="fa fa-users"></i> Users</a></li>
                <li><a href="{{ route('admin.subscriptions.list') }}"><i class="fa fa-credit-card"></i> Subscriptions</a></li>
                <li><a href="{{ route('admin.application.index') }}"><i class="fa fa-mobile-phone"></i> Applications</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    @yield('content')
</div>

@include('static.footer')
</body>
</html>
