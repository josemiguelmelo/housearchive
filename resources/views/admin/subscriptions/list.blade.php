@extends('admin.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h3>Subscriptions</h3>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table id="users" class="table text-sm table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Monthly Price ($)</th>
                    <th>Yearly Discount (%)</th>
                    <th>Discount (%)</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Monthly Price ($)</th>
                    <th>Yearly Discount (%)</th>
                    <th>Discount (%)</th>
                    <th>Options</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach($subscriptions as $subscription)
                    <tr style="font-size: 80%;">
                        <td>{{ $subscription->name }}</td>
                        <td>{{ $subscription->monthly_price }}</td>
                        <td align="center">{{ $subscription->yearly_discount }}</td>
                        <td align="center">{{ $subscription->discount }}</td>
                        <td align="right">
                            <div class="btn-toolbar" role="toolbar" aria-label="...">
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                    <a href="{{ route('admin.subscriptions.view', array('id' => $subscription->id)) }}" class="btn btn-info">
                                        <i class="fa fa-info"></i>
                                    </a>
                                    <button type="button" class="btn btn-warning">
                                        <i class="fa fa-lock"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#users').DataTable();
        } );
    </script>
@endsection
