@extends('admin.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h3>Subscription Info</h3>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-10">
                            <h3>{{ $subscription->name }}</h3>
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-warning" id="edit-subscription"><i class="fa fa-pencil"></i></button>
                        </div>
                        <div class="col-md-1">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('subscriptions.destroy', ['id' => $subscription->id]) }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="btn btn-danger" id="delete-book"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>


                    </div>

                </div>
                <div class="panel-body">
                    <div class="row">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        @if (Session::has('success'))
                            <div class="alert alert-success col-md-offset-1 col-md-10">
                                {{Session::get('success')}}
                            </div>
                        @endif
                        <div class="col-md-10 col-md-offset-1">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('subscriptions.update', ['id' => $subscription->id]) }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="control-label">Name</label>
                                <input type="text" class="form-control" name="name" id="name" value="{{ $subscription->name }}" disabled>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Books</label>
                                <input type="text" class="form-control" name="books" id="books" value="{{ $subscription->books }}" disabled>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Music</label>
                                <input type="text" class="form-control" name="music" id="music" value="{{ $subscription->music }}" disabled>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Movies</label>
                                <input type="text" class="form-control" name="movies" id="movies" value="{{ $subscription->movies }}" disabled>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Monthly Price ($)</label>
                                <input type="text" class="form-control" name="monthly_price" id="monthly_price" value="{{ $subscription->monthly_price }}" disabled>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Yearly Discount (%)</label>
                                <input type="text" class="form-control" name="yearly_discount" id="yearly_discount" value="{{ $subscription->yearly_discount }}" disabled>
                                Yearly Price: {{ $subscription->yearlyPrice() }}$
                            </div>


                            <div class="form-group">
                                <label class="control-label">Discount (%)</label>
                                <input type="text" class="form-control" name="discount" id="discount" value="{{ $subscription->discount }}" disabled>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Edit
                                    </button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>

        $("#edit-subscription").click(function(){
            $("input").prop("disabled", false);
        });

    </script>
@endsection
