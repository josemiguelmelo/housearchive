@extends('admin.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h3>Users</h3>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table id="users" class="table text-sm table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Subscription</th>
                    <th>Register Date</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Subscription</th>
                    <th>Register Date</th>
                    <th>Options</th>
                </tr>
                </tfoot>
                <tbody>
                    @foreach($users as $user)
                        <tr style="font-size: 80%;">
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td align="center">{{ $user->currentSubscription->name }}</td>
                            <td align="center">{{ date_format($user->created_at, 'd-m-Y') }}</td>
                            <td align="right">
                                <div class="btn-toolbar" role="toolbar" aria-label="...">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                        <a href="{{ route('admin.users.view', array('id' => $user->id)) }}" class="btn btn-info">
                                            <i class="fa fa-info"></i>
                                        </a>
                                        <button type="button" class="btn btn-warning">
                                            <i class="fa fa-lock"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#users').DataTable();
        } );
    </script>
@endsection
