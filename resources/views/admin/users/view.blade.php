@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-10">
            @include('static.go_back', ['url' => route('admin.users')])
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <h3>User Info</h3>
        </div>
        <div class="col-md-2">
            <button id="edit_user_info" class="btn btn-warning">Edit</button>
        </div>
    </div>
    <hr>
    <div class="row">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        @if (Session::has('success'))
            <div class="alert alert-success col-md-offset-1 col-md-10">
                {{Session::get('success')}}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.users.update', ['id' => $user->id]) }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    Name:
                <input class="form-control" value="{{ $user->name }}" disabled>
                <br>

                Email:
                <input class="form-control" value="{{ $user->email }}" disabled>
                <br>

                Register Date:
                <input class="form-control" value="{{ date_format($user->created_at, 'd/m/Y') }}" disabled>
                <br>

                Admin:
                    @if($user->admin)
                        <i class="fa fa-check fa-2x text-success"></i>
                    @else
                        <i class="fa fa-times fa-2x text-danger"></i>
                    @endif
                    <input name="set_admin" id="set_as_admin" type="checkbox"  @if($user->admin) checked value="1" @else value="0" @endif hidden disabled>
                <br>
                <br>
                Subscription Expiration:
                <input type="date" name="subscription_expiration" class="form-control" value="{{ $user->subscription_end }}" disabled>
                <br>

                Subscription Plan:
                <select id="subscription_plan" class="form-control" disabled name="subscription">
                    @foreach(\HouseArchive\Subscription::all() as $subscription)
                        <option value="{{ $subscription->id }}" @if($subscription->id == $user->currentSubscription->id) selected @endif>
                            {{ $subscription->name }}
                        </option>
                    @endforeach
                </select>

                <div id="save_edited_info_row" class="row" hidden>
                    <br>
                    <div class="col-md-2 col-md-offset-10">
                        <button id="save_edited_info" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <script>
        $("#edit_user_info").click(function(){
            $("input[type='date']").prop("disabled", false);
            $("input[type='checkbox']").prop("disabled", false);
            $("select").prop("disabled", false);
            $("#save_edited_info_row").show();
            $("#set_as_admin").show();
        });
    </script>
@endsection
