<div class="progress">
    @if(Auth::user()->currentSubscription->books != -1)
        <div class="progress-bar
                                @if(Auth::user()->diskUsage($resource) < 60)
                progress-bar-success
            @elseif(Auth::user()->diskUsage($resource) < 80)
                progress-bar-warning
            @else
                progress-bar-danger
            @endif" role="progressbar"
             aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
             style="width: {{ Auth::user()->diskUsage('books') }}%;">

            {{ Auth::user()->diskUsage($resource) }}%
        </div>
    @else
        <div class="progress-bar progress-bar-success" role="progressbar"
             aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
             style="width: 100%;">
            Unlimited Space
        </div>
    @endif

</div>