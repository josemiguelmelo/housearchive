@extends('app')

@section('content')

    <style>
        #calendar
        {
            width: 800px;
            margin: 0 auto;
        }
    </style>

    <div class="row">
        <div class="col-md-10">
            <h3>My Own Series</h3>
            @include('static.go_back', ['url' => route('home')])
        </div>
        <div class="col-md-2 ">
            <a href="{{ route('series.search') }}" class="btn btn-success">Search New Serie</a>
        </div>
    </div>

    <hr>
    <br>

    <div ng-app="application" ng-controller="SeriesController">


        @if (Session::has('success'))
            <div class="alert alert-success col-md-offset-1 col-md-10">
                {{Session::get('success')}}
            </div>
        @endif

        <button class="btn btn-sm btn-primary" ng-click="initCalendar()">View calendar</button>
        <div class="row" id="calendar-div">
            <div class="col-md-12">
                <div id='calendar'></div>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="input-group stylish-input-group">
                    <input type="text" ng-model="searchBar" class="form-control"  placeholder="Search" >
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-search"></span>
                </span>
                </div>
            </div>
        </div>

        <br><br>


        <div class="row">
            <div class="col-md-3" dir-paginate="serie in series | orderBy:'title' | filter: searchBar | itemsPerPage:12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="height: 13em; overflow: scroll;">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<% serie.image %>">
                            </div>
                            <div class="col-md-8">
                                <h3><% serie.name %></h3>
                                <% serie.rating %>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a ng-href="/series/show/<%serie.id%>">Open</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <dir-pagination-controls
                    max-size="5"
                    direction-links="true"
                    boundary-links="true" >
            </dir-pagination-controls>

        </div>

    </div>

@endsection
