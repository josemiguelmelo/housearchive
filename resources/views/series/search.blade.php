@extends('app')

@section('content')

    <div class="row">
        <div class="col-md-10">
            <h3>Series</h3>
            @include('static.go_back', ['url' => route('series')])
        </div>
    </div>

    <hr>
    <br>

    <div ng-app="application">


        @if (Session::has('success'))
            <div class="alert alert-success col-md-offset-1 col-md-10">
                {{Session::get('success')}}
            </div>
        @endif

        <div class="row"  ng-controller="SeriesController">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="input-group stylish-input-group">
                        <input type="text" ng-model="searchBar" class="form-control"  placeholder="Search" >
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-search"></span>
                </span>
                    </div>
                </div>
            </div>

            <br><br>

            <div class="row">
                <div class="col-md-3" dir-paginate="serie in searchResults | orderBy:'title' | filter: searchBar | itemsPerPage:10">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="height: 13em; overflow: scroll;">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="img-responsive" src="<% serie.image %>">
                                </div>
                                <div class="col-md-8">
                                    <h3><% serie.name %></h3>
                                    <% serie.rating %>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a ng-href="/series/show/<%serie.id%>">Open</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <dir-pagination-controls
                        max-size="5"
                        direction-links="true"
                        boundary-links="true" >
                </dir-pagination-controls>

            </div>
        </div>


    </div>
@endsection
