@extends('app')

@section('content')
    <div class="row">


        @include('static.go_back', ['url' => route('series')])

        <div class="col-md-10">
            @if(\HouseArchive\Series::where('tvmaze_id' , '=', $tvmaze_id)->where('user_id','=', Auth::user()->id)->first())
                <form method="post" action="{{ route('series.destroy' , [ 'id' => $tvmaze_id ]) }}" class=" text-right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="btn btn-warning">Unfollow</button>
                </form>
            @else
                <form method="post" action="{{ route('series.store') }}" class=" text-right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <input type="hidden" name="tvmaze_id" value="{{ $tvmaze_id }}">
                    <button class="btn btn-success">Follow</button>
                </form>
            @endif
        </div>
    </div>
    <div class="row">
        @if (Session::has('success'))
            <div class="alert alert-success col-md-offset-1 col-md-10">
                {{Session::get('success')}}
            </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <hr>
    <div id="serie-id" data-id="{{ $tvmaze_id }}" hidden></div>
    <div class="row" ng-app="application" ng-controller="SerieController">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-md-3">
                    <img src="<% serie.image %>" class="img img-responsive">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-8">
                            <h3><% serie.name %></h3>
                        </div>
                        <div class="col-md-4">
                            <div class="text-right">
                                <span class="label label-warning">Rating:  <% serie.rating %></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div ng-bind-html="serie.description"></div>
                        </div>
                    </div>
                </div>
            </div>
            <br><hr>

            <div class="row">
                <div class="col-md-12">
                    <h3>Episodes</h3>
                    <br>

                    <div ng-repeat="(number, episodesSeason)  in serie.episodes | groupBy: 'season'">

                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse<%number%>">
                                            <h3>Season <% number %> </h3>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse<%number%>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-1">
                                                <div class="row" ng-repeat="episode in episodesSeason">
                                                    <br>
                                                    <hr>
                                                    <div class="col-md-4">
                                                        <img src="<% episode.image.original %>" class="img img-responsive">
                                                    </div>
                                                    <div class="col-md-6 col-md-offset-1">
                                                        <h4><% episode.name %></h4>
                                                        <b>Release date:</b> <% episode.airdate %>
                                                        <b>Release hour:</b> <% episode.airtime %>
                                                        <br>
                                                        <a href="<% episode.url %>">More info!</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <hr>
    <br>
@endsection
