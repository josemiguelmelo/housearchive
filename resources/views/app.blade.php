<html>
<head>
	<meta name="viewport" content="width=device-width">

	<meta name="online_counter" ip="{{ $_SERVER['REMOTE_ADDR'] }}" url="{{ route('online') }}" token="{{ csrf_token() }}">

	<title>HouseArchive</title>

	<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
	<link href='/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/css/font-awesome.min.css">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="/js/jquery.min.js"></script>
	<script src="/js/jquery.cookie.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/chartjs.min.js"></script>
	<script src="/js/angular/angular.min.js"></script>
	<script src="/js/angular/angular-cookies.min.js"></script>
	<script src="/js/angular/dir-paginate.js"></script>
	<script src="/js/angular-application.js"></script>
	<script src="/js/controllers/bookscontroller.js"></script>
	<script src="/js/controllers/moviescontroller.js"></script>
	<script src="/js/controllers/musiccontroller.js"></script>
	<script src="/js/controllers/seriescontroller.js"></script>
	<script src="/js/controllers/serieController.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="/js/moment.min.js"></script>

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.5.0/fullcalendar.min.css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.5.0/fullcalendar.min.js"></script>

	<script src="/js/utils/online.js"></script>
	<script>
		online();
	</script>


@if(Route::current()->getName() == 'home')

	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/responsive.css">
	<link rel="stylesheet" href="/css/extras/animate.css">

	<link rel="stylesheet" href="/css/extras/lightbox.css">


	<script src="/js/lightbox.min.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/smooth-scroll.js"></script>
@else
	<style>
		body{
			padding-top: 80px;
		}
	</style>
@endif
</head>
<body>

<div class="logo-menu">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header col-md-3">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ route('home') }}">HouseArchive</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				</ul>
				<ul class="nav navbar-nav navbar-right">
					@if(Auth::check())
						@if(Auth::user()->admin)
							<li><a href="{{ route('admin.dashboard') }}">Admin Dashboard</a></li>
						@endif
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								{{ Auth::user()->name }} <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="{{ url('profile') }}"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
								<li><a href="{{ route('subscription.change') }}"><i class="fa fa-credit-card" aria-hidden="true"></i> Subscription</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="{{ url('auth/logout') }}">
										<i class="fa fa-power-off"></i> Logout
									</a></li>
							</ul>
						</li>
					@else
						<li><a href="{{ url('auth/login') }}">Sign In</a></li>
					@endif
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	</div>

	<div @if(Auth::check()) class="container" @endif>
		@if(Auth::check())
			@if(Auth::user()->sub_expiration_alert)
				<div id="subscription_expired_alert" class="alert alert-warning" role="alert">
					Subscription Expired!<br><br>
					<a class="btn btn-sm btn-primary" href="{{ route('profile') }}">Subscribe back!</a>
					<button id="dismiss_subscription_alert_button" class="btn btn-sm btn-danger" data-url="{{ route('dismiss_subscription_alert') }}">Dismiss</button>
				</div>
			@endif
		@endif

		@yield('content')
	</div>

	@include('static.footer')

<script>
	$("#dismiss_subscription_alert_button").click(function(){
		var dismissUrl = $(this).data("url");
		$.ajax({
			url:dismissUrl,
			type: "POST",
			data: { "_token" : "{{ csrf_token() }}"},
			success: function(){
				$("#subscription_expired_alert").hide();
			}
		});
	});
</script>
</body>
</html>
