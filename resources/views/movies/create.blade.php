@extends('app')

@section('content')

    @include('static.go_back', ['url' => route('movies')])
    <hr>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New Movie</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('movies.store') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">Title</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}" autocomplete="off" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Director</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="director" id="director" value="{{ old('director') }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">Date Published</label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="release-date" id="release-date" value="{{ old('release-date') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Description</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="description" id="description">{{ old('description') }}</textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">Shelf</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="shelf" id="shelf" value="{{ old('shelf') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Image</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="image" id="image" value="{{ old('image') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Image Preview</label>
                            <div class="col-md-6">
                                <img id="image-preview" src=""/>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>

    <script>
        $("#image").change(function(){
            $("#image-preview").attr("src", $(this).val());
        });

        var titlesComplete = [];
        var lastQuery = [];

        $("#title").change(function(){
            var imdbApi = "http://www.omdbapi.com/?plot=short&r=json&i=";

            var imdbMovie = "http://www.omdbapi.com/?plot=short&r=json&t=";
            var query = $(this).val().split(' ').join('+');

            $.ajax({
                type: "GET",
                url : imdbMovie + query,
                success : function(data)
                {
                    console.log(data);

                    if(!data.Error)
                    {
                        $("#description").val(data.Plot);
                        $("#director").val(data.Director);
                        var date = new Date(data.Released);



                        var day = ("0" + date.getDate()).slice(-2);
                        var month = ("0" + (date.getMonth() + 1)).slice(-2);
                        var finalDate = date.getFullYear()+"-"+(month)+"-"+(day) ;
                        $("#release-date").val(finalDate);
                    }
                }
            });
        });

        $("#title").blur(function(){
            $("#release-date").val(new Date(lastQuery.Search[0].Year));
            $("#image").val(lastQuery.Search[0].Poster);
            $("#image-preview").attr("src", lastQuery.Search[0].Poster);

        });


        var googleBooksAPI = "https://www.googleapis.com/books/v1/volumes?q=isbn:";

        $("#isbn").change(function(){
            var isbn = $("#isbn").val();
            $.ajax({
                url: googleBooksAPI + isbn,
                type: "GET",
                success: function(data)
                {
                    var volumeInfo = data.items[0].volumeInfo;

                    $("#title").val(volumeInfo.title);
                    $("#author").val(volumeInfo.authors[0]);
                    $("#publisher").val(volumeInfo.publisher);
                    $("#release-date").val(volumeInfo.publishedDate);
                    $("#description").val(volumeInfo.description);
                    $("#image").val(volumeInfo.imageLinks.thumbnail);
                    $("#image-preview").attr("src", volumeInfo.imageLinks.thumbnail);
                }
            });
        });

    </script>

@endsection
