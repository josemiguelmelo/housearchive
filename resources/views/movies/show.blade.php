@extends('app')

@section('content')

    @include('static.go_back', ['url' => route('movies')])

    <hr>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-8">
                            <h3>{{ $movie->title }}</h3>
                        </div>
                        <div class="col-md-2">
                            <span class="label label-warning" id="imdb-rating"></span>
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-info" id="edit-book"><i class="fa fa-pencil"></i></button>
                        </div>
                        <div class="col-md-1">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('movies.destroy', ['id' => $movie->id]) }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="btn btn-danger" id="delete-book"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>


                    </div>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-4">
                            <img id="img-preview" src="{{ $movie->image }}" class="img-responsive img-thumbnail">
                        </div>
                    </div>
                    <br>
                    <br>

                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        @if (Session::has('success'))
                            <div class="alert alert-success col-md-offset-1 col-md-10">
                                {{Session::get('success')}}
                            </div>
                        @endif

                        <div class="col-md-10 col-md-offset-1">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('movies.update', ['id' => $movie->id]) }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label class="control-label">Title</label>
                                    <input type="text" class="form-control" name="title" id="title" value="{{ $movie->title }}" disabled>
                                </div>


                                <div class="form-group">
                                    <label class="control-label">Shelf</label>
                                    <input type="hidden" class="form-control" name="shelf" id="shelf" value="{{ $movie->shelf }}">
                                    <label id="shelf-label" class="label @if($movie->shelf != "") label-success @else label-danger @endif ">@if($movie->shelf != "") {{ $movie->shelf }} @else n/a @endif </label>

                                </div>

                                <div class="form-group">
                                    <label class="control-label">Director</label>
                                    <input type="text" class="form-control" name="director" id="director" value="{{ $movie->director }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Published Date</label>
                                    <input type="date" class="form-control" name="release-date" id="release-date" value="{{ $movie->release_date }}" disabled>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Image</label>
                                    <input type="text" class="form-control" name="image" id="image" value="{{ $movie->image }}" disabled>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Description</label>
                                    <textarea onloadeddata="do_resize(this)" class="form-control" name="description" id="description" style="resize: none;" disabled>{{ $movie->description }}</textarea>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Edit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <hr>
    <br>

    <div class="row">

    </div>

    <script>
        function imdbRating(){
            var imdbMovie = "http://www.omdbapi.com/?plot=short&r=json&t=";
            var query = "{{ $movie->title }}";

            $.ajax({
                type: "GET",
                url : imdbMovie + query,
                success : function(data)
                {
                    if(!data.Error)
                    {
                        $("#imdb-rating").html("IMDb Rating: " + data.imdbRating);
                    }
                }
            });
        }

        imdbRating();

        $("#image").change(function(){
            $("#img-preview").attr("src", $(this).val());
        });

        $("#edit-book").click(function(){
            $("input").prop("disabled", false);
            $("textarea").prop("disabled", false);
            $("#shelf").attr('type', 'text');
            $("#shelf-label").hide();
        });


        var observe;
        if (window.attachEvent) {
            observe = function (element, event, handler) {
                element.attachEvent('on'+event, handler);
            };
        }
        else {
            observe = function (element, event, handler) {
                element.addEventListener(event, handler, false);
            };
        }
        function init () {
            function resize (element) {
                element.style.height = 'auto';
                element.style.height = element.scrollHeight+'px';
            }
            /* 0-timeout to get the already changed text */
            function delayedResize (element) {
                window.setTimeout(function() { resize(element) }, 0);
            }
            var textareas = document.getElementsByTagName("textarea");
            for (i = 0; i < textareas.length; i++) {
                var textarea = textareas[i];
                observe(textarea, 'change', function() { resize(this) });
                observe(textarea, 'cut', function() { delayedResize(this) });
                observe(textarea, 'paste', function() { delayedResize(this) });
                observe(textarea, 'drop',function() { delayedResize(this) });
                observe(textarea, 'keydown', function() { delayedResize(this) });
                textarea.focus();
                textarea.select();
                resize(textarea);
            }
        }

        init();
    </script>

@endsection
