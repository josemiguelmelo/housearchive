@extends('app')

@section('content')

    <section id="hero-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">HouseArchive</h1>
                    <h2 class="subtitle">
                        Start keeping track of your books, musics, movies and TV Series
                    </h2>

                    <img class="col-md-6 col-sm-6 col-xs-12 animated fadeInLeft" src="/imgs/housearchive.png" alt="">

                    <div class="col-md-6 col-sm-6 col-xs-12 animated fadeInRight delay-0-5">
                        <p>

                        </p>
                        <a href="{{ url('auth/register') }}" class="btn btn-common btn-lg">Join Now!</a>
                        <a href="{{ url('auth/login') }}" class="btn btn-primary btn-lg">Already joined?</a>
                    </div>

                </div>

            </div>
        </div>
    </section>


    <section id="services">
        <div class="container text-center">
            <div class="row">
                <h1 class="title">Tracking</h1>
                <h2 class="subtitle">Keeping track of your books, musics, movies and TV Series never been so easy</h2>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="service-item">
                        <h3>Easy</h3>
                        <p>
                            Keeping track of your books, musics, movies and TV Series was never so easy.<br>
                            With an awesome interface, it is an experience with no precedents.
                        </p>
                    </div>
                </div>


                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="service-item">
                        <h3>Free</h3>
                        <p>
                            Best thing, you don't to pay anything to start using!<br>
                            We have a free plan which allows you to keep track of a great ammount of books, musics and movies.<br>
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="service-item">
                        <h3>Responsive</h3>
                        <p>
                            Our beautiful interface is fully responsive and prepared to be used on each device!
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>



    <section id="portfolio">
        <div class="container">
            <div class="row">
                <h1 class="title">Plans</h1>

                <!-- item -->
                <div class="col-md-4 text-center">
                    <div class="panel panel-danger panel-pricing">
                        <div class="panel-heading">
                            <i class="fa fa-desktop"></i>
                            <h3>Free</h3>
                        </div>
                        <div class="panel-body text-center">
                            <p><strong>$0 / Month</strong></p>
                        </div>
                        <ul class="list-group text-center">
                            <li class="list-group-item"><i class="fa fa-check"></i> Personal use</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> 200 Books</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> 200 Music</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> 200 Movies</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> Unlimited TV Shows</li>
                        </ul>
                        <div class="panel-footer">
                            <a class="btn btn-lg btn-block btn-danger" href="{{ asset('auth/register') }}">START NOW!</a>
                        </div>
                    </div>
                </div>
                <!-- /item -->

                <!-- item -->
                <div class="col-md-4 text-center">
                    <div class="panel panel-warning panel-pricing">
                        <div class="panel-heading">
                            <i class="fa fa-desktop"></i>
                            <h3>Silver</h3>
                        </div>
                        <div class="panel-body text-center">
                            <p><strong>$5 / Month</strong></p>
                        </div>
                        <ul class="list-group text-center">
                            <li class="list-group-item"><i class="fa fa-check"></i> Advanced use</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> 750 Books</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> 750 Music</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> 750 Movies</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> Unlimited TV Shows</li>
                        </ul>
                        <div class="panel-footer">
                            <a class="btn btn-lg btn-block btn-warning" href="{{ asset('auth/register') }}">START NOW!</a>
                        </div>
                    </div>
                </div>
                <!-- /item -->

                <!-- item -->
                <div class="col-md-4 text-center">
                    <div class="panel panel-success panel-pricing">
                        <div class="panel-heading">
                            <i class="fa fa-desktop"></i>
                            <h3>Gold</h3>
                        </div>
                        <div class="panel-body text-center">
                            <p><strong>$15 / Month</strong></p>
                        </div>
                        <ul class="list-group text-center">
                            <li class="list-group-item"><i class="fa fa-check"></i> Professional use</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> Unlimited Books</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> Unlimited Music</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> Unlimited Movies</li>
                            <li class="list-group-item"><i class="fa fa-check"></i> Unlimited TV Shows</li>
                        </ul>
                        <div class="panel-footer">
                            <a class="btn btn-lg btn-block btn-success" href="{{ asset('auth/register') }}">BUY NOW!</a>
                        </div>
                    </div>
                </div>
                <!-- /item -->

            </div>
        </div>
    </section>

@endsection
