@extends('app')

@section('content')

    @include('static.go_back', ['url' => route('music')])

    <hr>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New Music</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('music.store') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">Album Title</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Author</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="author" id="author" value="{{ old('author') }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">Date Published</label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="release-date" id="release-date" value="{{ old('release-date') }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">Shelf</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="shelf" id="shelf" value="{{ old('shelf') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Image</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="image" id="image" value="{{ old('image') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Image Preview</label>
                            <div class="col-md-6">
                                <img id="image-preview" src=""/>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>

    <script>
        $("#image").change(function(){
            $("#image-preview").attr("src", $(this).val());
        });

        var spotifyInitialAPI = "https://api.spotify.com/v1/search?q=";
        var spotifyAlbumType = "&type=album";


        $("#title").change(function(){
            var title= $(this).val();

            var replaced = title.split(' ').join('+');
            var finalURL = spotifyInitialAPI + replaced + spotifyAlbumType;

            $.ajax({
                type: "GET",
                url : finalURL,
                success : function(data)
                {
                    $("#image").val(data.albums.items[0].images[0].url);
                    $("#image-preview").attr("src", data.albums.items[0].images[0].url);
                }
            });
        });

    </script>

@endsection
