@extends('app')

@section('content')

    @include('static.go_back', ['url' => route('music')])

    <hr>

    <a href="" id="listen_spotify" class="btn btn-success"><i class="fa fa-spotify fa-2x"></i> Listen on Spotify</a>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-10">
                            <h3>{{ $music->title }}</h3>
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-warning" id="edit-book"><i class="fa fa-pencil"></i></button>
                        </div>
                        <div class="col-md-1">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('music.destroy', ['id' => $music->id]) }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="btn btn-danger" id="delete-book"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>


                    </div>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-4">
                            <img id="img-preview" src="{{ $music->image }}" class="img-responsive img-thumbnail">
                        </div>
                    </div>
                    <br>
                    <br>

                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        @if (Session::has('success'))
                            <div class="alert alert-success col-md-offset-1 col-md-10">
                                {{Session::get('success')}}
                            </div>
                        @endif

                        <div class="col-md-10 col-md-offset-1">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('music.update', ['id' => $music->id]) }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label class="control-label">Title</label>
                                    <input type="text" class="form-control" name="title" id="title" value="{{ $music->title }}" disabled>
                                </div>


                                <div class="form-group">
                                    <label class="control-label">Shelf</label>
                                    <input type="hidden" class="form-control" name="shelf" id="shelf" value="{{ $music->shelf }}">
                                    <label id="shelf-label" class="label @if($music->shelf != "") label-success @else label-danger @endif ">@if($music->shelf != "") {{ $music->shelf }} @else n/a @endif </label>

                                </div>

                                <div class="form-group">
                                    <label class="control-label">Author</label>
                                    <input type="text" class="form-control" name="author" id="author" value="{{ $music->author }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Published Date</label>
                                    <input type="date" class="form-control" name="release-date" id="release-date" value="{{ $music->release_date }}" disabled>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Image</label>
                                    <input type="text" class="form-control" name="image" id="image" value="{{ $music->image }}" disabled>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Edit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <hr>
    <br>

    <div class="row">

    </div>

    <script>

        var title= "{{ $music->title }}";
        var spotifyInitialAPI = "https://api.spotify.com/v1/search?q=";
        var spotifyAlbumType = "&type=album";

        var replaced = title.split(' ').join('+');
        var finalURL = spotifyInitialAPI + replaced + spotifyAlbumType;

        $.ajax({
            type: "GET",
            url : finalURL,
            success : function(data)
            {
                $("#listen_spotify").attr("href", data.albums.items[0].external_urls.spotify);
            }
        })

        $("#image").change(function(){
            $("#img-preview").attr("src", $(this).val());
        });

        $("#edit-book").click(function(){
            $("input").prop("disabled", false);
            $("textarea").prop("disabled", false);
            $("#shelf").attr('type', 'text');
            $("#shelf-label").hide();
        });

    </script>

@endsection
