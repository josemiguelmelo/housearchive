@extends('app')

@section('content')

    <div class="row">
        <div class="col-md-10">
            <h3>Music</h3>
            @include('static.go_back', ['url' => route('home')])
        </div>
        <div class="col-md-2 ">
            <a href="{{ route('music.create') }}" class="btn btn-success">Add Music</a>
        </div>
    </div>

    <hr>
    <br>

    <div ng-app="application">


        @if (Session::has('success'))
            <div class="alert alert-success col-md-offset-1 col-md-10">
                {{Session::get('success')}}
            </div>
        @endif

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="input-group stylish-input-group">
                    <input type="text" ng-model="searchBar" class="form-control"  placeholder="Search" >
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-search"></span>
                </span>
                </div>
            </div>
        </div>

        <br><br>


        <div class="row" ng-controller="MusicController">
            <div class="col-md-3" dir-paginate="music in musics | orderBy:'title' | filter: searchBar | itemsPerPage:10">
                <div class="panel panel-default">
                    <div class="panel-heading" style="height: 13em; overflow: scroll;">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<% music.image %>">
                            </div>
                            <div class="col-md-8">
                                <h3><% music.title %></h3>
                                <% music.author %>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <b>Shelf : </b>
                        <label class="label label-success" ng-if="music.shelf != ''"><% music.shelf %></label>
                        <label class="label label-danger" ng-if="music.shelf == ''">n/a</label>

                    </div>
                    <div class="panel-footer">
                        <a ng-href="/music/show/<%music.id%>">Open</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <dir-pagination-controls
                    max-size="5"
                    direction-links="true"
                    boundary-links="true" >
            </dir-pagination-controls>

        </div>

    </div>
@endsection
