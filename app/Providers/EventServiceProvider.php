<?php namespace HouseArchive\Providers;

use Carbon\Carbon;
use HouseArchive\Subscription;
use HouseArchive\User;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'event.name' => [
			'EventListener',
		],
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		User::creating(function($user)
		{
			$user->subscription_id = Subscription::where('default','=', true)->first()->id;
		});

		Subscription::creating(function($subscription)
		{
			Log::debug("subscription default: " . $subscription->default);
			if($subscription->default == true)
			{
				Subscription::where('default','=',true)->update(['default' => 0]);
			}
		});

	}

}
