<?php namespace HouseArchive;

use Illuminate\Database\Eloquent\Model;

class Book extends Model {

    protected $table = 'books';
    protected $fillable = ['shelf','user_id','isbn', 'title', 'author', 'publisher', 'image','release_date', 'description'];


}
