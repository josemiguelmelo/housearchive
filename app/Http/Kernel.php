<?php namespace HouseArchive\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'\LucaDegasperi\OAuth2Server\Middleware\OAuthExceptionHandlerMiddleware',

	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => 'HouseArchive\Http\Middleware\Authenticate',
		'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'HouseArchive\Http\Middleware\RedirectIfAuthenticated',
		'admin' => 'HouseArchive\Http\Middleware\AdminAuthenticated',

		'oauth' => 'HouseArchive\Http\Middleware\OAuthMiddleware',

		'csrf' => 'HouseArchive\Http\Middleware\VerifyCsrfToken',
	];

}
