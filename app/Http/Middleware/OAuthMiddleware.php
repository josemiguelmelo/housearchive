<?php namespace HouseArchive\Http\Middleware;

use Closure;
use HouseArchive\User;
use Authorizer;

class OAuthMiddleware {

	private $auth;

	public function __construct(Authorizer $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$token = $request->get('access_token');
		Authorizer::validateAccessToken();
		if(User::find(Authorizer::getResourceOwnerId()))
			return $next($request);
		return json_encode(['error' => true, 'msg' => 'invalid access token']);
	}

}
