<?php namespace HouseArchive\Http\Requests;

use Carbon\Carbon;
use HouseArchive\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OnlineUserRequest extends Request {

	public $expirationTime = 10;
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [

		];
	}


	public function generateClientId()
	{
		return uniqid();
	}

	public function online()
	{
		if($this->has('client_id'))
		{
			$client_id = $this->input('client_id');
		}else{
			$client_id = $this->generateClientId();
		}

		$browser = $this->input('browser');
		$ip = $this->input('ip');
		if(Auth::user())
		{
			$user_id = Auth::user()->id;
		}else{
			$user_id = null;
		}

		Log::info("User " . $ip . " became online");

		$user = DB::table('online_users')
			->where('client_id','=', $client_id)
			->first();

		if($user)
		{
			DB::table('online_users')
				->where('id','=', $user->id)
				->update(['expire_time' => Carbon::now()->addMinutes($this->expirationTime)]);
		}else{
			DB::table('online_users')->insert(
				[
					'client_id' => $client_id,
					'ip_address' => $ip,
					'browser' => $browser,
					'expire_time' => Carbon::now()->addMinutes($this->expirationTime),
					'user_id' => $user_id
				]
			);
		}

		return [
			'error' => false,
			'msg' => 'Online user registered.',
			'client_id' => $client_id,
			'expire_time' => $this->expirationTime
		];
	}

}
