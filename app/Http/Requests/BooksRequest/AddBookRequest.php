<?php namespace HouseArchive\Http\Requests\BooksRequest;

use HouseArchive\Book;
use HouseArchive\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class AddBookRequest extends Request
{

    private $user;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  => 'required',
            'author' => 'required',
        ];
    }

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);

        $this->user = Auth::user();
    }

    public function setUser($user){
        $this->user = $user;
    }

    public function addBook()
    {
        $book = new Book;

        $book->isbn = $this->input('isbn');
        $book->title = $this->input('title');
        $book->author = $this->input('author');
        $book->publisher = $this->input('publisher');
        if($this->input('image') != "")
            $book->image = $this->input('image');
        else
            $book->image = 'https://cdn0.iconfinder.com/data/icons/back-to-school/90/circle-school-learn-study-subject-literature-book-512.png';

        $book->release_date = $this->input('release-date');
        $book->description = $this->input('description');
        $book->shelf = $this->input('shelf');

        $book->user_id = $this->user->id;

        $book->save();
    }

}
