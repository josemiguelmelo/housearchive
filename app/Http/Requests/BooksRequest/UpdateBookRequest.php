<?php namespace HouseArchive\Http\Requests\BooksRequest;

use HouseArchive\Book;
use HouseArchive\Http\Requests\Request;

class UpdateBookRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'isbn'   => 'required',
            'title'  => 'required',
            'author' => 'required',
        ];
    }

    public function updateBook($id)
    {
        $book = Book::find($id);

        $book->isbn = $this->input('isbn');
        $book->title = $this->input('title');
        $book->author = $this->input('author');
        $book->publisher = $this->input('publisher');
        $book->image = $this->input('image');
        $book->release_date = $this->input('release-date');
        $book->description = $this->input('description');
        $book->shelf = $this->input('shelf');

        $book->save();
    }

}
