<?php namespace HouseArchive\Http\Requests;

use Carbon\Carbon;
use HouseArchive\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AddApplication extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::user()->admin == 1;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required',
			'secret' => 'required',
			'id' => 'required'
		];
	}

	public function addApplication()
	{
		DB::table('oauth_clients')->insert([
			'id' => $this->get('id'),
			'secret' => $this->get('secret'),
			'name' => $this->get('name'),
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
										   ]);
	}
}
