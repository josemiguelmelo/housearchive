<?php namespace HouseArchive\Http\Requests;

use HouseArchive\Http\Requests\Request;
use HouseArchive\User;
use Illuminate\Support\Facades\Auth;

class AdminUpdateUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::user()->admin;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'subscription' => 'numeric',
			'subscription_expiration' => 'required|date',
		];
	}

	public function updateUserInfo($id)
	{
		$user = User::find($id);

		if( $this->input('set_admin') == 1)
		{
			$admin = true;
		}
		else{
			$admin = false;
		}

		$user->subscription_id = $this->input('subscription');
		$user->subscription_end = $this->input('subscription_expiration');
		$user->admin = $admin;
		$user->save();
		return true;
	}

}
