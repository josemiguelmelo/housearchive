<?php namespace HouseArchive\Http\Requests\MusicRequest;

use HouseArchive\Book;
use HouseArchive\Http\Requests\Request;
use HouseArchive\Movies;
use HouseArchive\Music;
use Illuminate\Support\Facades\Auth;

class UpdateMusicRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  => 'required',
            'author' => 'required',
        ];
    }

    public function updateMusic($id)
    {
        $music = Music::find($id);

        $music->title = $this->input('title');
        $music->author = $this->input('author');
        $music->image = $this->input('image');
        $music->release_date = $this->input('release-date');
        $music->shelf = $this->input('shelf');

        $music->save();
    }

}
