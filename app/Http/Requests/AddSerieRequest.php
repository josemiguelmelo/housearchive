<?php namespace HouseArchive\Http\Requests;

use HouseArchive\Http\Requests\Request;
use HouseArchive\Series;
use Illuminate\Support\Facades\Auth;

class AddSerieRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'tvmaze_id' => 'required'
		];
	}

	public function addSerie()
	{
		$serie = new Series;

		$serie->user_id = Auth::user()->id;
		$serie->tvmaze_id = $this->input('tvmaze_id');
		$serie->save();
	}

}
