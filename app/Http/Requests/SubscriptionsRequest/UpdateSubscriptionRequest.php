<?php namespace HouseArchive\Http\Requests\SubscriptionsRequest;

use HouseArchive\Http\Requests\Request;
use HouseArchive\Subscription;

class UpdateSubscriptionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'   => 'required',
			'books'  => 'required',
			'music' => 'required',
			'movies' => 'required',
			'monthly_price' => 'required',
			'discount' => 'required',
			'yearly_discount' => 'required',
		];
	}

	public function updateSubscription($id)
	{
		$subscription = Subscription::find($id);

		$subscription->name = $this->input('name');
		$subscription->books = $this->input('books');
		$subscription->music = $this->input('music');
		$subscription->movies = $this->input('movies');
		$subscription->monthly_price = $this->input('monthly_price');
		$subscription->discount = $this->input('discount');
		$subscription->yearly_discount = $this->input('yearly_discount');

		$subscription->save();
	}

}
