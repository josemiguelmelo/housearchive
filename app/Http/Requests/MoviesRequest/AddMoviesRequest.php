<?php namespace HouseArchive\Http\Requests\MoviesRequest;

use HouseArchive\Book;
use HouseArchive\Http\Requests\Request;
use HouseArchive\Movies;
use Illuminate\Support\Facades\Auth;

class AddMoviesRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  => 'required',
            'director' => 'required',
        ];
    }

    public function addMovie()
    {
        $movie = new Movies;

        $movie->title = $this->input('title');
        $movie->director = $this->input('director');
        $movie->image = $this->input('image');
        $movie->release_date = $this->input('release-date');
        $movie->description = $this->input('description');
        $movie->shelf = $this->input('shelf');

        $movie->user_id = Auth::user()->id;

        $movie->save();
    }

}
