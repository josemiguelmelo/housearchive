<?php namespace HouseArchive\Http\Requests;

use HouseArchive\Http\Requests\Request;
use HouseArchive\User;
use Illuminate\Support\Facades\Hash;

class UpdateUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255',
			'password' => 'confirmed|min:6',
		];
	}

	public function updateUser($id){
		$user = User::find($id);

		if($user->email != $this->input('email'))
		{
			if(User::whereEmail($this->input('email'))->first())
			{
				return ['email' => 'Email already exists'];
			}

			$user->email = $this->input('email');
		}

		$user->name = $this->input('name');
        if(strlen($this->input('password')) >= 6)
		    $user->password = Hash::make($this->input('password'));

		$user->save();

		return ['success' => true];
	}

}
