<?php namespace HouseArchive\Http\Controllers;

use HouseArchive\Http\Requests;
use HouseArchive\Http\Controllers\Controller;

use HouseArchive\Series;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SeriesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('series.list');
	}

	public function search()
	{
		return view('series.search');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\AddSerieRequest $request)
	{
		$request->addSerie();

		Session::flash('success', "Serie followed successfully.");
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return view('series.show')
			->with('tvmaze_id', $id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$serie = Series::where('tvmaze_id', '=', $id)->first();

		$serie->delete();

		Session::flash('success', "Serie unfollowed successfully.");
		return redirect()->back();
	}

}
