<?php namespace HouseArchive\Http\Controllers;

use HouseArchive\Book;
use HouseArchive\Http\Requests;
use HouseArchive\Http\Controllers\Controller;

use HouseArchive\Music;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MusicController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('music.list');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('music.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\MusicRequest\AddMusicRequest $request)
	{

		if(Auth::user()->isAllowedToAddResource('music'))
		{
			$request->addMusic();
		}else{
			Session::flash('errors', "No music space.");
		}

		return redirect()->route('music');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return view('music.show')
			->with('music', Music::whereId($id)->first());
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Requests\MusicRequest\UpdateMusicRequest $request, $id)
	{
		$request->updateMusic($id);

		Session::flash('success', "Music updated successfully.");
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$music = Music::find($id);
		if($music->delete()) {
			Session::flash('success', "Music deleted successfully.");
			return redirect()->route('music');
		}
		return redirect()->back();
	}

}
