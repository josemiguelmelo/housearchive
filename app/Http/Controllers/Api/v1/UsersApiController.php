<?php namespace HouseArchive\Http\Controllers\Api\v1;

use HouseArchive\Book;
use HouseArchive\Http\Controllers\Controller;
use HouseArchive\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use LucaDegasperi\OAuth2Server\Authorizer;

class UsersApiController extends Controller
{

    private $authorizer;


    public function __construct(Authorizer $authorizer)
    {
        $this->middleware('oauth', [
            'except' => [
                'store',
            ]]);
        $this->authorizer = $authorizer;
    }

    public function show()
    {
        return User::find($this->authorizer->getResourceOwnerId());
    }

    public function store(Request $request)
    {
        User::create([
                         'name'     => $request->get('name'),
                         'email'    => $request->get('email'),
                         'password' => Hash::make($request->get('password')),
                         'admin'    => false,
                     ]);

        return response()->json(['error' => false, 'msg' => 'User registration succeeded.']);
    }

    public function update(Request $request)
    {
        $user = User::find($this->authorizer->getResourceOwnerId());

        $user->name = $request->has('name') ? $request->get('name') : $user->name;
        $user->email = $request->has('email') ? $request->get('email') : $user->email;
        $user->password = $request->has('password') ? Hash::make($request->get('password')) : $user->password;

        return response()->json(['error' => false, 'msg' => $user->save()]);
    }

    public function destroy($id)
    {

        Book::destroy($id);

        return response()->json(['error' => false, 'msg' => 'Book removed successfully.']);
    }
}
