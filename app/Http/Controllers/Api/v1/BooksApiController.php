<?php namespace HouseArchive\Http\Controllers\Api\v1;

use HouseArchive\Book;
use HouseArchive\Http\Controllers\Controller;
use HouseArchive\Http\Requests\BooksRequest\AddBookRequest;
use HouseArchive\Http\Requests\BooksRequest\UpdateBookRequest;
use HouseArchive\User;
use LucaDegasperi\OAuth2Server\Authorizer;

class BooksApiController extends Controller
{

    private $authorizer;

    public function __construct(Authorizer $authorizer)
    {
        $this->middleware('oauth');

        $this->authorizer = $authorizer;
    }

    public function index()
    {
        return json_encode(Book::where('user_id', '=', User::find($this->authorizer->getResourceOwnerId())->id)->get());
    }

    public function show($id){
        return Book::find($id);
    }

    public function store(AddBookRequest $request)
    {
        $user = User::find($this->authorizer->getResourceOwnerId());

        $request->setUser($user);
        $request->addBook();

        return response()->json(['error' => false, 'msg' => 'Book inserted successfully.']);
    }

    public function update(UpdateBookRequest $request, $id)
    {
        $request->updateBook($id);

        return response()->json(['error' => false, 'msg' => 'Book updated successfully.']);
    }

    public function destroy($id){
        Book::destroy($id);
        return response()->json(['error' => false, 'msg' => 'Book removed successfully.']);
    }
}
