<?php namespace HouseArchive\Http\Controllers;

use HouseArchive\Http\Requests;
use HouseArchive\Http\Controllers\Controller;

use HouseArchive\Http\Requests\MoviesRequest\UpdateMovieRequest;
use HouseArchive\Movies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MoviesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		return view('movies.list');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return view('movies.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\MoviesRequest\AddMoviesRequest $request)
	{

		if(Auth::user()->isAllowedToAddResource('movies'))
		{
			$request->addMovie();
		}else{
			Session::flash('errors', "No movies space.");
		}

		return redirect()->route('movies');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return view('movies.show')
			->with('movie', Movies::whereId($id)->first());
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UpdateMovieRequest $request, $id)
	{
		$request->updateMovie($id);

		Session::flash('success', "Movie updated successfully.");
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$movie = Movies::find($id);
		if($movie->delete()) {
			Session::flash('success', "Movie deleted successfully.");
			return redirect()->route('movies');
		}
		return redirect()->back();
	}

}
