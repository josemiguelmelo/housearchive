<?php namespace HouseArchive\Http\Controllers;

use HouseArchive\Http\Requests;
use HouseArchive\Http\Controllers\Controller;

use HouseArchive\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SubscriptionsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Requests\SubscriptionsRequest\UpdateSubscriptionRequest $request, $id)
	{
		$request->updateSubscription($id);

		Session::flash('success', "Subscription updated successfully.");
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$subscription = Subscription::find($id);
		if($subscription->delete()) {
			Session::flash('success', "Subscription deleted successfully.");
			return redirect()->route('admin.subscriptions.list');
		}
		return redirect()->back();
	}

}
