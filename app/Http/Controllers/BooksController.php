<?php namespace HouseArchive\Http\Controllers;

use HouseArchive\Book;
use HouseArchive\Http\Requests;
use HouseArchive\Http\Controllers\Controller;

use HouseArchive\Http\Requests\BooksRequest\AddBookRequest;
use HouseArchive\Http\Requests\BooksRequest\UpdateBookRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BooksController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('books.list')
			->with('books', Book::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('books.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AddBookRequest $request)
	{
		if(Auth::user()->isAllowedToAddResource('books'))
		{
			$request->addBook();
		}else{
			Session::flash('errors', "No books space.");
		}
		return redirect()->route('books');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return view('books.show')
			->with('book', Book::whereId($id)->first());
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UpdateBookRequest $request , $id)
	{
		$request->updateBook($id);
		Session::flash('success', "Book updated successfully.");
		return redirect()->back();
	}

	public function api(Request $request){
		return json_encode(\HouseArchive\Book::where('user_id', '=', \Illuminate\Support\Facades\Auth::user()->id)
							   ->get());
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$book = Book::find($id);
		if($book->delete()) {
			Session::flash('success', "Book deleted successfully.");
			return redirect()->route('books');
		}
		return redirect()->back();
	}


	public function uploadPage(){
		return view('books.upload');
	}

	public function upload(Request $request)
	{
		$counter = 0;

		$file = $request->file('books_csv');

		$books_array = [];
		if (($handle = fopen($file->getPathname(), "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
				if($counter> 0){
					$array = [];
					$array["title"] = $data[0];
					$array["author"] = $data[1];
					$array["shelf"] = $data[2];
					$array["image"] = 'https://cdn0.iconfinder.com/data/icons/back-to-school/90/circle-school-learn-study-subject-literature-book-512.png';
					$array["user_id"] =  Auth::user()->id;

					$books_array[] = $array;
				}
				$counter++;
			}
			fclose($handle);
		}

		DB::table('books')->insert($books_array);
		return redirect()->route('books');
	}
}
