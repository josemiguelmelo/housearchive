<?php namespace HouseArchive\Http\Controllers;

use HouseArchive\Http\Requests;
use HouseArchive\Http\Controllers\Controller;

use HouseArchive\Http\Requests\OnlineUserRequest;
use HouseArchive\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller {


	public function online(OnlineUserRequest $request)
	{
		Log::info("here");
		return $request->online();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update( Requests\UpdateUserRequest $request, $id)
	{
		$result = $request->updateUser($id);

		if(isset($result['success'])) {
			Session::flash('success', 'User information updated successfully.');
			return redirect('profile');
		}

		return view('profile')
			->withErrors($result);
	}


	public function profile()
	{
		return view('profile');
	}

	public function dismissSubscriptionAlert()
	{
		Auth::user()->sub_expiration_alert = false;
		Auth::user()->save();

		return json_encode(['error' => false, 'msg' => 'Subscription expiration alert dismissed.']);
	}

	public function changeSubscription()
    {
        return view('subscription.subscribe');
    }


	public function subscription(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $token = $input['stripe_token'];

        if($user->subscribed())
        {
            $user->subscription($input['subscription_plan'])->swap();

            $user->updateSubscription($input['subscription_plan']);

            return redirect()->route('profile')->with('subscription_success','Subscription replaced successfully.');
        }

        try {
            $user->subscription($input['subscription_plan'])->create($token,[
                'email' => $user->email
            ]);

            $user->updateSubscription($input['subscription_plan']);

            return redirect()->route('profile')->with('subscription_success','Subscription is completed.');
        } catch (Exception $e) {
            return back()->with('success',$e->getMessage());
        }
    }

}
