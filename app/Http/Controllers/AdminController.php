<?php namespace HouseArchive\Http\Controllers;

use HouseArchive\Http\Requests;
use HouseArchive\Http\Controllers\Controller;

use HouseArchive\Subscription;
use HouseArchive\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller {

	public function dashboard()
	{
		return view('admin.dashboard');
	}

	public function users()
	{
		return view('admin.users.list')
			->with('users', User::all());
	}

	public function usersView($id)
	{
		return view('admin.users.view')
			->with('user', User::find($id));
	}

	public function userUpdate(Requests\AdminUpdateUserRequest $request, $id)
	{
		$request->updateUserInfo($id);
		Session::flash('success', "User information updated successfully.");
		return redirect()->back();
	}

	public function subscriptionsList()
	{
		return view('admin.subscriptions.list')
			->with('subscriptions', Subscription::all());
	}

	public function subscriptionsView($id)
	{
		return view('admin.subscriptions.view')
			->with('subscription', Subscription::whereId($id)->first());
	}
}
