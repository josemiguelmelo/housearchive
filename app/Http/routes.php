<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::post('books/add_new', ['as' => 'books.add_new', 'uses' => 'BooksController@store']);

Route::post('online', array('as' => 'online', 'uses' => 'UsersController@online'));

Route::get('/', array('as' => 'home', 'uses' => 'WelcomeController@index'));


Route::post('oauth/access_token', function() {
	return Response::json(Authorizer::issueAccessToken());
});

Route::group(['prefix' => 'api'], function(){
    Route::group(['prefix' => 'v1'], function(){
        require('api/routes.php');
    });
});


Route::group(['middleware' => 'auth'], function () {

	Route::post('dismiss_subscription_alert', ['as'=> 'dismiss_subscription_alert', 'uses' => 'UsersController@dismissSubscriptionAlert']);

	Route::get('home', ['as' => 'dashboard', 'uses' => 'HomeController@index']);
	Route::get('profile', ['as' => 'profile', 'uses' => 'UsersController@profile']);

	Route::post('users/update/{id}',['as'=> 'users.update', 'uses' => 'UsersController@update']);

	Route::get('books', ['as' => 'books', 'uses' => 'BooksController@index']);
	Route::get('books/create', ['as' => 'books.create', 'uses' => 'BooksController@create']);
	Route::get('books/show/{id}', ['as' => 'books.show', 'uses' => 'BooksController@show']);
	Route::post('books/store', ['as' => 'books.store', 'uses' => 'BooksController@store']);
	Route::post('books/update/{id}', ['as' => 'books.update', 'uses' => 'BooksController@update']);
	Route::post('books/destroy/{id}', ['as' => 'books.destroy', 'uses' => 'BooksController@destroy']);
	Route::post('books/upload', ['as' => 'books.upload', 'uses' => 'BooksController@upload']);
	Route::get('books/upload_page', ['as' => 'books.upload_page', 'uses' => 'BooksController@uploadPage']);

	Route::get('movies', ['as' => 'movies', 'uses' => 'MoviesController@index']);
	Route::get('movies/create', ['as' => 'movies.create', 'uses' => 'MoviesController@create']);
	Route::get('movies/show/{id}', ['as' => 'movies.show', 'uses' => 'MoviesController@show']);
	Route::post('movies/store', ['as' => 'movies.store', 'uses' => 'MoviesController@store']);
	Route::post('movies/update/{id}', ['as' => 'movies.update', 'uses' => 'MoviesController@update']);
	Route::post('movies/destroy/{id}', ['as' => 'movies.destroy', 'uses' => 'MoviesController@destroy']);


	Route::get('music', ['as' => 'music', 'uses' => 'MusicController@index']);
	Route::get('music/create', ['as' => 'music.create', 'uses' => 'MusicController@create']);
	Route::get('music/show/{id}', ['as' => 'music.show', 'uses' => 'MusicController@show']);
	Route::post('music/store', ['as' => 'music.store', 'uses' => 'MusicController@store']);
	Route::post('music/update/{id}', ['as' => 'music.update', 'uses' => 'MusicController@update']);
	Route::post('music/destroy/{id}', ['as' => 'music.destroy', 'uses' => 'MusicController@destroy']);


	Route::get('series', ['as' => 'series', 'uses' => 'SeriesController@index']);
	Route::get('series/search', ['as' => 'series.search', 'uses' => 'SeriesController@search']);
	Route::get('series/create', ['as' => 'series.create', 'uses' => 'SeriesController@create']);
	Route::get('series/show/{id}', ['as' => 'series.show', 'uses' => 'SeriesController@show']);
	Route::post('series/store', ['as' => 'series.store', 'uses' => 'SeriesController@store']);
	Route::post('series/update/{id}', ['as' => 'series.update', 'uses' => 'SeriesController@update']);
	Route::post('series/destroy/{id}', ['as' => 'series.destroy', 'uses' => 'SeriesController@destroy']);


    Route::get('subscription/change', [ 'as' => 'subscription.change', 'uses' => 'UsersController@changeSubscription']);
    Route::post('subscription', [ 'as' => 'subscription', 'uses' => 'UsersController@subscription']);

	Route::group(['prefix' => 'api'], function() {
		Route::get('books', ['as' => 'api.books', 'uses' => 'BooksController@api']);

		Route::get('movies', ['as' => 'api.movies', 'uses' => function(){
			return json_encode(\HouseArchive\Movies::where('user_id', '=', \Illuminate\Support\Facades\Auth::user()->id)->get());
		}]);

		Route::get('music', ['as' => 'api.movies', 'uses' => function(){
			return json_encode(\HouseArchive\Music::where('user_id', '=', \Illuminate\Support\Facades\Auth::user()->id)->get());
		}]);

		Route::get('series', ['as' => 'api.series', 'uses' => function(){
			return json_encode(\HouseArchive\Series::where('user_id', '=', \Illuminate\Support\Facades\Auth::user()->id)->get());
		}]);

		Route::get('serie/{id}', ['as' => 'api.serie', 'uses' => function($id){
			return json_encode(\HouseArchive\Series::where('user_id', '=', \Illuminate\Support\Facades\Auth::user()->id)
								   ->where('id','=', $id)
								   ->first());
		}]);

	});



	Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function(){
		Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'AdminController@dashboard']);
		Route::get('users', ['as' => 'admin.users', 'uses' => 'AdminController@users']);
		Route::get('users/{id}', ['as' => 'admin.users.view', 'uses' => 'AdminController@usersView']);
		Route::post('users/{id}', ['as' => 'admin.users.update', 'uses' => 'AdminController@userUpdate']);

		Route::get('subscriptions', ['as' => 'admin.subscriptions.list', 'uses' => 'AdminController@subscriptionsList']);
		Route::get('subscriptions/{id}', ['as' => 'admin.subscriptions.view', 'uses' => 'AdminController@subscriptionsView']);

		Route::post('subscriptions/update/{id}', ['as' => 'subscriptions.update', 'uses' => 'SubscriptionsController@update']);
		Route::post('subscriptions/destroy/{id}', ['as' => 'subscriptions.destroy', 'uses' => 'SubscriptionsController@destroy']);

		Route::get('application/create', ['as' => 'admin.application.create', 'uses' => 'ApplicationController@create']);
		Route::post('application/store', ['as' => 'admin.application.store', 'uses' => 'ApplicationController@store']);
		Route::get('application/all', ['as' => 'admin.application.index', 'uses' => 'ApplicationController@index']);
		Route::get('application/destroy/{id}', ['as' => 'admin.application.destroy', 'uses' => 'ApplicationController@destroy']);

	});
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
