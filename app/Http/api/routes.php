<?php

header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type, x-xsrf-token' );

// BOOKS endpoints
Route::get('books', 'Api\v1\BooksApiController@index');
Route::post('books', 'Api\v1\BooksApiController@store');
Route::get('books/{id}', 'Api\v1\BooksApiController@show');
Route::post('books/{id}', 'Api\v1\BooksApiController@update');
Route::delete('books/{id}', 'Api\v1\BooksApiController@destroy');

// USERS endpoints
Route::get('users', 'Api\v1\UsersApiController@show');
Route::post('users', 'Api\v1\UsersApiController@store');
Route::post('users/edit', 'Api\v1\UsersApiController@update');