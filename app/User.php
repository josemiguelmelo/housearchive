<?php namespace HouseArchive;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, BillableContract
{

	use Authenticatable, CanResetPassword;
    use Billable;

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'admin'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function currentSubscription()
    {
        return $this->belongsTo('HouseArchive\Subscription','subscription_id','id');
    }

	public function books()
	{
		return $this->hasMany('HouseArchive\Book');
	}

	public function movies()
	{
		return $this->hasMany('HouseArchive\Movies');
	}

	public function music()
	{
		return $this->hasMany('HouseArchive\Music');
	}

	public function series()
	{
		return $this->hasMany('HouseArchive\Series');
	}


	public function diskUsage($modelName)
	{
	    $diskUsage = ($this->currentSubscription->$modelName != -1) ?
            (count($this->$modelName) / $this->currentSubscription->$modelName) * 100
            : 0;
        $diskUsage = $diskUsage > 100 ? 100 : $diskUsage;
        return  number_format($diskUsage, 2);
	}


	public function isAllowedToAddResource($resource)
	{
        if($this->currentSubscription->$resource == -1)
            return true;
		return count($this->$resource) < $this->currentSubscription->$resource;
	}

    public function updateSubscription($code){

        $subscription = Subscription::where('code','=', $code)->first();

        $this->currentSubscription()->associate($subscription);
        $this->save();
    }

}
