<?php namespace HouseArchive;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Subscription extends Model  {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subscriptions';

    public $timestamps = false;

    public function yearlyPrice()
    {
        return ($this->monthly_price * 12) -
        (($this->yearly_discount/100) * ($this->monthly_price) * 12);
    }

}
