<?php namespace HouseArchive;

use Illuminate\Database\Eloquent\Model;

class Series extends Model {

	protected $table = "series";

	public $timestamps = false;

}
