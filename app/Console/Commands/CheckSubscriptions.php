<?php namespace HouseArchive\Console\Commands;

use Carbon\Carbon;
use DateTime;
use HouseArchive\Subscription;
use HouseArchive\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CheckSubscriptions extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'subscription:check';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check all subscriptions.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		Log::info('Checking subscriptions expiration');
		$users = User::where('subscription_end', '<=', new DateTime('today'))->get();
		$default_subscription = Subscription::where('default', '=', true)->first();

		foreach($users as $user)
		{
			if($user->subscription->monthly_price > 0)
			{
				$user->subscription_id = $default_subscription->id;
				$user->sub_expiration_alert = true;
			}
			$user->subscription_end = Carbon::today()->addMonths(1);
			$user->save();
		}

		$this->info("All subscriptions checked!");
		Log::info('All subscriptions expiration checked.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
